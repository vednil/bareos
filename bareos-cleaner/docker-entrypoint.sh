#!/usr/bin/env bash

sed -i "s/address = .*/address = $BAREOS_DIRECTOR_HOST/g" /etc/bareos/bconsole.conf
sed -i "s/Password = .*/Password = \"$BAREOS_DIRECTOR_PASSWORD\"/g" /etc/bareos/bconsole.conf


exec "$@"
