#!/bin/bash

DIR="/mnt/bareos"

# Remove purged volumes
while read -r line
do
    volume=$(echo $line | awk '{ print $1 }')
    pool=$(echo $line | awk '{ print $2 }')
    echo "delete volume=$volume yes" | bconsole
    rm -f $DIR/$pool/$volume
done < <(echo "list volume" | bconsole | grep Purged | awk '{ print $4,$27 }')

# Remove missing volumes
for pool in `ls -w 1 $DIR`; do
    for volume in `find $DIR/$pool -maxdepth 1 -type f -printf "%f\n"`; do
        echo "list volume=$volume" | bconsole | if grep --quiet "No results to list"; then
            echo "$volume is ready to be deleted"
            rm -f $DIR/$pool/$volume
        fi
    done
done

