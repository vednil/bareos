#!/usr/bin/env bash

# Director Configuration
if [ ! "$(ls -A /etc/bareos/bareos-dir.d)" ]
then

    dpkg-reconfigure bareos-director

    # Database parameters
    sed -i "s/dbuser = .*/dbuser = \"${DB_USER}\"/g" /etc/bareos/bareos-dir.d/catalog/MyCatalog.conf
    sed -i "s/dbpassword = .*/dbpassword = \"${DB_PASSWORD}\"/g" /etc/bareos/bareos-dir.d/catalog/MyCatalog.conf
    sed -i "/dbpassword/a \ \ dbaddress = \"${DB_HOST}\"" /etc/bareos/bareos-dir.d/catalog/MyCatalog.conf

    # Director password
    sed -i "s/Password = .*/Password = \"${BAREOS_DIRECTOR_PASSWORD}\"/g" /etc/bareos/bareos-dir.d/director/bareos-dir.conf

    # Webui configuration
    if [ ! -f /etc/bareos/bareos-dir.d/console/admin.conf ]
    then
        curl -s https://raw.githubusercontent.com/bareos/bareos/master/webui/install/bareos/bareos-dir.d/console/admin.conf.example --output /etc/bareos/bareos-dir.d/console/admin.conf
    fi
    if [ ! -f /etc/bareos/bareos-dir.d/profile/webui-admin.conf ]
    then
        curl -s https://raw.githubusercontent.com/bareos/bareos/master/webui/install/bareos/bareos-dir.d/profile/webui-admin.conf --output /etc/bareos/bareos-dir.d/profile/webui-admin.conf
    fi
    sed -i "s/Password = .*/Password = \"${BAREOS_WEBUI_PASSWORD}\"/g" /etc/bareos/bareos-dir.d/console/admin.conf

fi

# Create database
export PGHOST=${DB_HOST}
export PGUSER=${DB_USER}
export PGPASSWORD=${DB_PASSWORD}
if [ -z "$(psql -lqt | awk -F "|" '{print $1}' | grep bareos)" ]
then
    sleep 5
    /usr/lib/bareos/scripts/create_bareos_database
    /usr/lib/bareos/scripts/make_bareos_tables
    /usr/lib/bareos/scripts/grant_bareos_privileges
else
    /usr/lib/bareos/scripts/update_bareos_tables
    /usr/lib/bareos/scripts/grant_bareos_privileges
fi

chown -R bareos:bareos /etc/bareos/bareos-dir.d


exec "$@"
