#!/bin/bash
# Как пользоваться: запустить скрипт "./bareos-script.sh CLIENT JOB SCHEDULE TYPE ARG"
# Значение переменной ARG задается в зависимости от значения переменной TYPE. Например,
# для базы данных PostgreSQL указываем адрес базы данных, а для MSSQL - имя конкретной базы.
# Пример: ./bareos-script.sh bareos-client 1c-postgres-DB DailyFull pg_basebackup 1c-postgres
# Пример: ./bareos-script.sh win-client win-DATA DailyIncremental files "C:\SER"

# Mongo
# Создаем суперпользователя:
# db.createUser({user: "root", pwd: "password", roles: [{role: "userAdminAnyDatabase", db: "admin
# Создаем пользователя для резервного копирования:
# db.createUser({user: "backup", pwd: "password", roles: [{role: "backup", db: "admin"}]})
# Команда для выполнения резервного копирования Mongo:
# mongodump -h 192.168.0.1 --username backup --password password --forceTableScan --archive=backup.archive

# PostgreSQL
# Создаем пользователя для резервного копирования:
# CREATE ROLE backup REPLICATION LOGIN ENCRYPTED PASSWORD 'password';
# Добавляем в файл pg_hba.conf строку:
# host replication backup 192.168.0.1/32 md5
# Применяем изменения:
# SELECT pg_reload_conf();
# Настраиваем параметры репликации:
# sed -i "s/^#wal_level = .*/wal_level = replica/" /var/lib/postgresql/data/postgresql.conf
# sed -i "s/^#max_wal_senders = .*/max_wal_senders = 2/" /var/lib/postgresql/data/postgresql.conf
# Перезапускаем postgres.

# MySQL
# Создаем пользователя для резервного копирования:
# GRANT LOCK TABLES, SELECT ON *.* TO 'backup'@'%' IDENTIFIED BY 'password';
# GRANT SHOW VIEW ON *.* TO 'backup'@'%';

CLIENT=$1
JOB=$2
SCHEDULE=$3
TYPE="$4"
ARG=$5

BAREOS_DIRECTOR_CONF="/etc/bareos/bareos-dir.d"
BAREOS_STORAGE_CONF="/etc/bareos/bareos-sd.d"


# Создаем конфиг хранилища для bareos-storage
cat > $BAREOS_STORAGE_CONF/device/$JOB.conf <<EOF
Device {
  Name = $JOB
  Media Type = File
  Archive Device = /mnt/bareos/$JOB
  LabelMedia = yes;                   # lets Bareos label unlabeled media
  Random Access = yes;
  AutomaticMount = yes;               # when device opened, read it
  RemovableMedia = no;
  AlwaysOpen = no;
  Description = "File device. A connecting Director must have the same Name and MediaType."
}
EOF
docker exec bareos-storage sh -c "mkdir /mnt/bareos/$JOB && chown -R bareos:bareos /mnt/bareos/$JOB"
docker exec bareos-storage sh -c "chmod 750 /mnt/bareos/$JOB && chmod 640 /mnt/bareos/$JOB/*"


# Добавляем конфиг хранилища для bareos-director
cat > $BAREOS_DIRECTOR_CONF/storage/$JOB.conf <<EOF
Storage {
  Name = $JOB
  Address = bareos-storage
  Password = "changeme"
  Device = $JOB
  Media Type = File
}
EOF
cat > $BAREOS_DIRECTOR_CONF/pool/$JOB.conf <<EOF
Pool {
  Name = $JOB
  Pool Type = Backup
  Recycle = no                       # Bareos can automatically recycle Volumes
  AutoPrune = yes                     # Prune expired volumes
  Volume Retention = 90 days         # How long should the Full Backups be kept? (#06)
  Maximum Volume Jobs = 1
  Label Format = "\${Pool}_\${Year}-\${Month:p/2/0/r}-\${Day:p/2/0/r}_\${Hour:p/2/0/r}-\${Minute:p/2/0/r}"
}
EOF


# Проверяем тип бэкапа
ADD_RUN_SCRIPT=FALSE
if [ "$TYPE" == "pg_basebackup" ]; then
	FILE="/mnt/bareos/backup/$JOB"
	RunBefore="pg_basebackup -Xf -Ft -z -h $ARG -U backup -D /mnt/bareos/backup/%n"
	RunAfter="rm -rf /mnt/bareos/backup/%n"
	ADD_RUN_SCRIPT=TRUE
elif [ "$TYPE" == "mysqldump" ]; then
	FILE="/mnt/bareos/backup/$JOB.sql"
	RunBefore="sh -c 'mysqldump --single-transaction -h ${ARG} --all-databases > ${FILE}'"
	RunAfter="rm -f $FILE"
	ADD_RUN_SCRIPT=TRUE
elif [ "$TYPE" == "mongo" ]; then
    FILE="/mnt/bareos/backup/$JOB.archive"
    RunBefore="mongodump -h $ARG --username backup --password changeme --forceTableScan --archive=$FILE"
    RunAfter="rm -f $FILE"
    ADD_RUN_SCRIPT=TRUE
elif [ "$TYPE" == "mssql" ]; then
	FILE="mssqlvdi:instance=SQLEXPRESS:database=$ARG"
fi


# Добавляем fileset
cat > $BAREOS_DIRECTOR_CONF/fileset/$JOB.conf <<EOF
FileSet {
  Name = "$JOB"
  Include {
    Options {
      signature = MD5
      compression = gzip
    }
    File = "$FILE"
  }
}
EOF
if [ "$TYPE" == "files" ]; then
    sed -i "s/File =./#File/g" $BAREOS_DIRECTOR_CONF/fileset/$JOB.conf
    IFS=';'
    read -a files <<< "$ARG"
    for file in "${files[@]}"
    do
        sed -i "/#File/i \ \ \ \ File = \"$file\"" $BAREOS_DIRECTOR_CONF/fileset/$JOB.conf
    done
elif [ "$TYPE" == "mssql" ]; then
	sed -i "s/File =/Plugin =/g" $BAREOS_DIRECTOR_CONF/fileset/$JOB.conf
    sed -i '/compression = gzip/d' $BAREOS_DIRECTOR_CONF/fileset/$JOB.conf
    sed -i '/Include {/i \ \ Enable VSS = no' $BAREOS_DIRECTOR_CONF/fileset/$JOB.conf
fi


# Добавляем job
cat > $BAREOS_DIRECTOR_CONF/job/$JOB.conf <<EOF
Job {
  Name = "$JOB"
  Client = "$CLIENT"

  FileSet = "$JOB"
  Storage = "$JOB"
  Pool = "$JOB"

  Type = Backup
  Level = "Full"
  Schedule = "$SCHEDULE"
  Messages = Standard
  Priority = 10
  Write Bootstrap = "/var/lib/bareos/%c.bsr"

EOF
if [ "$ADD_RUN_SCRIPT" == TRUE ]; then
	cat >> $BAREOS_DIRECTOR_CONF/job/$JOB.conf <<EOF

  # This creates a dump of our database in the local filesystem on the client
  RunScript {
    FailJobOnError = Yes
    RunsOnClient = Yes
    RunsWhen = Before
    Command = "$RunBefore"
  }

  # This deletes the dump in our local filesystem on the client
  RunScript {
    RunsOnSuccess = Yes
    RunsOnClient = Yes
    RunsWhen = After
    Command = "$RunAfter"
  }
EOF
fi
cat >> $BAREOS_DIRECTOR_CONF/job/$JOB.conf <<EOF
}
EOF
