#!/usr/bin/env bash

sed -i "s/Password = .*/Password = \"${BAREOS_CLIENT_PASSWORD}\"/g" /etc/bareos/bareos-fd.d/director/bareos-dir.conf

echo "*:*:*:$POSTGRES_BACKUP_USER:$POSTGRES_BACKUP_PASSWORD" > /root/.pgpass
chmod 600 /root/.pgpass
echo -e "[mysqldump]\nuser=$MYSQL_BACKUP_USER\npassword=$MYSQL_BACKUP_PASSWORD\ncolumn-statistics=0" > /root/.my.cnf
chmod 600 /root/.my.cnf


exec "$@"
