#!/usr/bin/env bash

sed -i "s/\[localhost-dir\]/\[${BAREOS_DIRECTOR_HOST}\]/g" /etc/bareos-webui/directors.ini
sed -i "s/diraddress = \"localhost\"/diraddress = \"${BAREOS_DIRECTOR_HOST}\"/g" /etc/bareos-webui/directors.ini


exec "$@"
