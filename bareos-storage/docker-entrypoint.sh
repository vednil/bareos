#!/usr/bin/env bash

if [ ! "$(ls -A /etc/bareos/bareos-sd.d)" ]
then

    dpkg-reconfigure bareos-storage

    sed -i "s/Password = .*/Password = \"${BAREOS_STORAGE_PASSWORD}\"/g" /etc/bareos/bareos-sd.d/director/bareos-dir.conf

fi

chown -R bareos:bareos /etc/bareos/bareos-sd.d


exec "$@"
